import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { JsonEditorComponent, JsonEditorOptions } from 'ang-jsoneditor';
import { Base64 } from 'js-base64';

@Component({
	selector: 'app-encoder-decoder-container',
	templateUrl: './encoder-decoder-container.component.html',
	styleUrls: ['./encoder-decoder-container.component.css']
})
export class EncoderDecoderContainerComponent implements OnInit {
	@ViewChildren('jsonEditor') editorSelector: QueryList<JsonEditorComponent>;

	textEditor: any;
	codeEditor: any;

	textEditorConfig = new JsonEditorOptions();
	codeEditorConfig = new JsonEditorOptions();

	textEditorValue: any = '';
	codeEditorValue: any = '';

	constructor() {

		this.textEditorConfig.mode = 'text';
		this.codeEditorConfig.mode = 'code';

		this.textEditorConfig.statusBar = this.codeEditorConfig.statusBar = true;
	}

	decodeData(data) {
		if (!Base64.isValid((data))) {
			return data;
		}
		const ndbKeyRegex = new RegExp('\\$[a-zA-Z0-9\\-]+$');
		const decodeBase64 = (encodedData: string): any => {
			try {
				let decodedData = atob(decodeURIComponent(unescape(encodedData)));
				if (ndbKeyRegex.test(decodedData)) {
					return encodedData;
				}
				return decodedData;
			}
			catch {
				return encodedData;
			}
		}
		if (data) {
			if (typeof (data) === 'string') {
				return decodeBase64(data);
			} else {
				let tmp = JSON.parse(data);
				if (tmp.data) {
					return JSON.parse(decodeBase64(tmp.data));
				} else {
					return JSON.parse(decodeBase64(tmp));
				}
			}
		}
		else {
			return data;
		}
	}

	onKeyUp(event: any) {
		if ((event.ctrlKey || event.metaKey) && event.which == 13) {
			event.preventDefault();
			this.onClickDecodeBtn();
			return false;
		}
	}

	setCodeEditorValue(valueData: any, skipDecoding: boolean = false) {
		let dataToSet = skipDecoding !== false ? valueData : this.decodeData(valueData);
		this.codeEditor.editor.setText(dataToSet);
		this.codeEditor.editor.repair();

		if (this.hasJsonStructure(this.codeEditor.editor.getText())) {
			this.codeEditor.editor.mode = 'code';
			this.codeEditor.editor.format();
		} else {
			this.codeEditor.editor.mode = 'text';
		}
		this.codeEditor.editor.focus();
	}

	hasJsonStructure(valueData: any) {
		if (typeof valueData !== 'string') return false;
		try {
			const result = JSON.parse(valueData);
			const type = Object.prototype.toString.call(result);
			return type === '[object Object]'
				|| type === '[object Array]';
		} catch (err) {
			return false;
		}
	}

	setTextEditorValue(valueData: any) {
		this.textEditor.editor.setText(valueData);
	}

	onClickDecodeBtn() {
		this.setCodeEditorValue(this.textEditor.editor.getText());
	}

	ngOnInit() {

	}

	ngAfterViewInit() {
		let editorElementArr = this.editorSelector.toArray();
		this.textEditor = editorElementArr[0];
		this.codeEditor = editorElementArr[1];

		this.textEditor.editor.errorTable.errorTableVisible = false;

		let startText = 'eyAnSGVsbG8nIDogJ1dvcmxkJyB9';
		this.setTextEditorValue(startText);
		this.setCodeEditorValue(startText)
	}

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EncoderDecoderContainerComponent } from './encoder-decoder-container.component';

describe('EncoderDecoderContainerComponent', () => {
  let component: EncoderDecoderContainerComponent;
  let fixture: ComponentFixture<EncoderDecoderContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EncoderDecoderContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EncoderDecoderContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

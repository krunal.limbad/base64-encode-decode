import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EncoderDecoderContainerComponent } from './encoder-decoder-container/encoder-decoder-container.component';

const routes: Routes = [
  { path: '', component: EncoderDecoderContainerComponent, pathMatch: 'full' },
  { path: '**', redirectTo: '/' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
